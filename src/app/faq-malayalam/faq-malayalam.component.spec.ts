import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaqMalayalamComponent } from './faq-malayalam.component';

describe('FaqMalayalamComponent', () => {
  let component: FaqMalayalamComponent;
  let fixture: ComponentFixture<FaqMalayalamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaqMalayalamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaqMalayalamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
