import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-faq-malayalam',
  templateUrl: './faq-malayalam.component.html',
  styleUrls: ['./faq-malayalam.component.css']
})
export class FaqMalayalamComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $.getScript('../../assets/js/ta.js')
  }

}
