import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnRefundyComponent } from './return-refundy.component';

describe('ReturnRefundyComponent', () => {
  let component: ReturnRefundyComponent;
  let fixture: ComponentFixture<ReturnRefundyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReturnRefundyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnRefundyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
