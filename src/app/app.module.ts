import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ServicesComponent } from './services/services.component';
import { FaqComponent } from './faq/faq.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FaqMalayalamComponent } from './faq-malayalam/faq-malayalam.component';
import { OpenAccountComponent } from './open-account/open-account.component';
import { BetaComponent } from './beta/beta.component';
import { CareersComponent } from './careers/careers.component';
import { AdvisoryServiceComponent } from './advisory-service/advisory-service.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ReturnRefundyComponent } from './return-refundy/return-refundy.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PaymentComponent } from './payment/payment.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { GammaComponent } from './gamma/gamma.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ServicesComponent,
    FaqComponent,
    HeaderComponent,
    FooterComponent,
    FaqMalayalamComponent,
    OpenAccountComponent,
    BetaComponent,
    CareersComponent,
    AdvisoryServiceComponent,
    DisclaimerComponent,
    PrivacyPolicyComponent,
    ReturnRefundyComponent,
    TermsConditionsComponent,
    PaymentComponent,
    EnquiryComponent,
    GammaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
