import { LeadingComment } from '@angular/compiler';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { from } from 'rxjs';
import { AdvisoryServiceComponent } from './advisory-service/advisory-service.component';
import { BetaComponent } from './beta/beta.component';
import { CareersComponent } from './careers/careers.component';
import { DisclaimerComponent } from './disclaimer/disclaimer.component';
import { EnquiryComponent } from './enquiry/enquiry.component';
import { FaqMalayalamComponent } from './faq-malayalam/faq-malayalam.component';
import { FaqComponent } from './faq/faq.component';
import { GammaComponent } from './gamma/gamma.component';
import { HomeComponent } from './home/home.component';
import { OpenAccountComponent } from './open-account/open-account.component';
import { PaymentComponent } from './payment/payment.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ReturnRefundyComponent } from './return-refundy/return-refundy.component';
import { ServicesComponent } from './services/services.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'  },
  { path: 'home', component: HomeComponent },
  { path: 'services', component: ServicesComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'faq-malayalam', component: FaqMalayalamComponent },
  { path: 'openaccount', component: OpenAccountComponent },
  { path: 'beta', component: BetaComponent  },
  { path: 'careers', component: CareersComponent },
  { path: 'advisory', component: AdvisoryServiceComponent },
  { path: 'disclaimer', component: DisclaimerComponent },
  { path: 'privacy', component: PrivacyPolicyComponent },
  { path: 'return', component: ReturnRefundyComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'terms', component: TermsConditionsComponent },
  { path: 'gamma', component: GammaComponent },
  { path: 'enquiry', component: EnquiryComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


