import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { from } from 'rxjs';
import { ElementRef } from '@angular/core'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    public selector: ElementRef
  ) { }

  ngOnInit(){
    $.getScript('../../assets/js/main.js','../../assets/vendor/jquery.easing/jquery.easing.min.js','../../assets/vendor/counterup/counterup.min.js','../../assets/vendor/venobox/venobox.min.js','../../assets/vendor/owl.carousel/owl.carousel.min.js')
    }

    
  scrolltwo(){
    let item = this.selector.nativeElement.querySelector('#about');
    item.scrollIntoView({ behavior: 'smooth', block: 'center'});
  }

  }
